/*dentro del array hay que declarar 20 unsigned char*/
#include <stdio.h>
#include <stdlib.h>

#define MAX 20

int main (int argc, char *argcv[] ) {

 unsigned char l[MAX]; // El array lo llamamos l con un máximo de 20 celdas, Unsigned char es que le damos un valor de 0 - 255 el valor de las celdas

 for (int i=0; i<MAX; i++) // for que sirve para hacer los calculos
     // l[i]= i + 1;
      l[i]= (i+1)*(i+1);

 for (int i=0; i<MAX; i++) //donde imprimimos
      printf ("  %i |", l[i]);
      printf ("\n");
 
    
  return EXIT_SUCCESS;
}
