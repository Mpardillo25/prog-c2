#include <stdio.h>
#include <stdlib.h>

#define P(...) printf ( "MACRO:" __VA_ARGS__); /* -----------------------------------------------------MACRO-----------------------------------------------------------------------
                                       es una macro poerque al lado hay unos paréntesis con puntos y cuando pongamos una P ya sabe que es un printf y no hay que escribirlo, V
                                    -----------------------------------------------    FUNCIÓN VARIÁDICA -------------------------------------------------------------------------
                                     P(...) significa que ponga todos los parámetros que hayas pasado en el printf (__VA_ARGS__) que eso significa  VA_ARGS, que es todo lo que pongas.*/

                                    //A una macro se le puede pasar un parámetro P(x) printf ( (x) * 5 );
int main(int argc, char *argv[]){
    
    int b = 37;
    b %= 5; //Operador RESTO es como b= b % 5

    P("%%: %i\n",b);

    b <<= 2; //desplazo 2bit hacia la izquierda ej: 110 --> 11000
    P("<<:%i\n",b);

    return EXIT_SUCCESS;
}
