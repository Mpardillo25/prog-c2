#include <stdio.h>
#include <unistd.h>
#define MAXCOLS 100

int main () {
  /*Las mismas tres lineas de tres formas distintas*/
  printf("Hola \x0A \x09 Beep \x7 \xA");
  printf("Hola\n \t Beep \a \n");
   printf("Hola \012 \011 Beep \007 \012");

  printf("Hola \n \0esto es secreto");// "/0 es en octal y se entiende como es que un 0 y no sale lo demás"Hay un HOLA luego hay un " espacio " y después finaliza la cadena de caracteres  
  
  printf("\n");
  for(int vez=0; vez<MAXCOLS; vez ++){
   printf("\r"); //retornar la línea
      for(int igual=0; igual<vez; igual++)
        printf("=");
      printf("> %2i%%",vez); //2i imprime 2 caracteres enteros
      fpurge(stdout); // buffer
      sleep (1);
  }
  printf("\n.FIN.");
  return 0;
}
