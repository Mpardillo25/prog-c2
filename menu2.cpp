#include <stdio.h>
#include <stdlib.h>

enum TipoOpcion {suma, resta}; //constantes de tipo enumeración

int menu () { //la funcion menu devuelve lo que vale opcion
    int opcion;
    system("clear");
    system("toilet -fpagga --gay MENU 2");

    printf(
"\n"    //para no poner comillas se puede poner así \n\ entonces anularía la sentencia siguiente.
"   MENU\n"
"   ====\n"
"\n"
"\n         1.-Suma.\n"
"\n         2.- Resta.\n"
"       Opcion: "
          );
   
    scanf ("%i", &opcion);

    return opcion - 1; // con esto hago que suma = 0 y resta =0 entonces hay que restar 1 porque si escojo la opcion 2 sería 1, y si escojo la opcion 1 es 0.
}

int main () {
   int op1 = 7, op2 = 5;
   int opcion = menu ();
   
   printf ("La opcion elegida es: %i\n", opcion);
   
   switch(opcion) { 
       case suma:
           printf ("%i + %i = %i \n", op1, op2, op1 + op2);
          break;
       case resta:
           printf ("%i - %i = %i \n", op1, op2, op1 - op2);
          break;

       default:
          fprintf (stderr,"Tu eres tonto, macho \n"); //stderr es el tubo de errores,  que con fprintf pone el mensaje en un fichero o tubo, que manda el stderr
          return EXIT_FAILURE; // te devuelve un error cuando haces en la termianl un echo $?
   }
   return EXIT_SUCCESS;

}
