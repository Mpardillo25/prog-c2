#include <stdio.h>
#include <stdlib.h>
#include <string.h> //strcpy: copia, strncpy

#define MAX_LINEAS 0x100 //256

const char * const cprog ="cadenas";
char vprog[]= "cadenas";


/*FUNCION STRLEN CASERA*/
void concatena (char *destino, const char *origen, int max){
    
   // Avanzar destino hasta \0
    while (*destino != '\0') destino ++;
   
    /****************************************PARA METER EL MENSAJE SECRETO******************************************/
    destino ++;



    /* while (*(dest++) != \0);  Es lo mismo que el primer while y ese ++: es de posfijo, 1º mira lo que vale desc y luego lo incrementa
      for (; *dest != \0; dest++);
    */


   // Copiar caracter a caracter de org a destino como mucho max o hasta \0
    for(int cuenta=0; *origen!='\0' && cuenta<max; cuenta++, ++origen, ++destino)
      *destino = *origen; //mete lo que hay en la direccion de memoria dest lo que hay en la otra direccion de memoria origen
    
    *destino = *origen;//fuera del bucle y hay que hacer una copia    
}
void cifra (char *frase, int clave){

    while (*frase != '\0')
        *(frase++) += clave;
}



int main (int argc, char *argv[]) {
  
  char linea [MAX_LINEAS];
    strcpy (linea, cprog); //solo funciona con const char*
    strncpy (linea, cprog, MAX_LINEAS); //asegura que solo copie n caracteres y el programa no reviente.
    strcat (linea, " ");
    strncat (linea, vprog, MAX_LINEAS - strlen(linea));//strcat: concatena, pone lo que hay en vprog a continuacion de lo que hay en linea, y strlen: cantidad de caracteres que tiene una frase descontando el \0
  concatena (linea, vprog, MAX_LINEAS - strlen(linea));
  
  cifra (linea + strlen(linea) + 1, 3);
  
  printf ("%s\n", linea);
  printf ("Mensaje cifrado: %s\n", linea + strlen(linea) + 1); /*Mensaje secreto*/

 cifra (linea +strlen(linea) +1, -3);
 printf("Mensaje descifrado: %s\n", linea + strlen(linea) +1);

  return EXIT_SUCCESS;

}
