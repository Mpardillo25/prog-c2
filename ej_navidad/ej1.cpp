#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>

void titulo () {
    system ("clear");
    system ("toilet --metal -fpagga COLOR");
    printf ("\n\n");
}

unsigned char pregunta_intensidad(const char *componente) {
    unsigned int entrada;
    
   do{
    __fpurge(stdin); //vacia lo que hay en el tubo y te hace una pregunta para que el tubo no se rompa por el usuario al preguntarle   
     printf ("Intensidad de %s[0-255]: ",componente);
     scanf ("%u", &entrada);
   } while(entrada > 0xFF);

    return (unsigned char) entrada;
}

int main (int argc, char * argv[]) {
    unsigned char r,g,b;  //unsigned, vale para numeros positivos y va desde el 0 a 255 y cada color utiliza 1 byte
    unsigned char xr;
   
    titulo(); //hay que llamar a la funcion para que se ejecute
    r= pregunta_intensidad ("rojo");

    g= pregunta_intensidad ("verde");
    
    b= pregunta_intensidad ("azul");

    xr = r ^ 0xFF; //255

    printf ("#%X%X%X => #%X%X%X\n",r,g,b,xr,g,b);

   return EXIT_SUCCESS;

}
