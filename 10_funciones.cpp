#include <stdio.h>
#include <stdlib.h>

double f(double x) { return x * x - 3; }// {funcion}

int main (int argc, char *argv[]) {

    for (double x=-5; x<5; x+=0.5) //es preferible que iniciemos las variables dentro del for.
        printf("x: %2lf, f(x): %.2lf \n", x, f(x)); //f retorna un valor que es x * x -3

    return EXIT_SUCCESS;

}
