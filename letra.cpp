#include <stdio.h>
#define SALTO ('a' - 'A')
int main (){
  /*Queremos que la letra c = 0x063 minuscula se convierta en C mayúsucla*/
    char letra = 'c';
    
    /*1. opcione:
     * -  letra= letra - 0x20; //0x63(c) - 0x43(C) = 0x20 distancia 
     *    entre la mayúscula y la minúscula
     *    si la letra es mayúscula
    letra = letra - ('a'-'A');*/
    
    /*1.2.opcion:
     * -   operador taquigráfico letra = letra - n; = letra -= n
     *
     * si la letra es mayúscula
          letra -= SALTO;*/

    /* si no lo es*/
    /*    letra += SALTO ;*/

    if (letra >= 'a' && letra <= 'z')
          letra -= SALTO;
    else
        letra += SALTO;

    printf("Letra mayúscula:%c \n", letra);

    return 0;




}
