#include <stdio.h>
#include <stdlib.h>
  
  const char *simbolo ="*"; //direccion  de un caracter asterísco

int main(int argc, char *argv[]) {

   //char simbolo = '*';
   unsigned altura;
   printf("Altura: ");
   scanf(" %u", &altura);
 
  for (int f=0; f<altura; f++){
      for(int c=0; c<altura; c++)
         if(f==0 || c==0 || f == altura - 1 || c == altura -1)  
          printf("%s", simbolo);
         else
            printf(" ");
      //Imprimir un salto de línea
      printf("\n");
   }
    return EXIT_SUCCESS;
  
  }
 
