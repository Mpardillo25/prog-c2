 #include <stdio.h>
 #include <stdlib.h>
 //programa que diga que n divide exactamente a ese numero, que al dividirlos les quede resto 0
 
int main (int argc, char *argv[]){
     unsigned tope;
     printf ("Introduzca un nº tope: ");
     scanf(" %u", &tope);
  
     for (unsigned dvsr= tope; dvsr>0; dvsr--){ //dvsr: divisor
         printf ("Divisores de  %u: ",dvsr);
          for(unsigned pd= dvsr/2; pd>0; pd--) //pd: posible divisor
            if(!(dvsr % pd )) // pd % dvsr ==0
             printf ("%u ", pd);
          printf("\n");
     }
     return EXIT_SUCCESS;
  }

